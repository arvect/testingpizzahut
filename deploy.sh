rm -rf src/github.com
rm -rf src/golang.org
rm -rf src/gopkg.in
rm -rf src/cloud.google.com

GOPATH=$(echo $(pwd)) go get -u github.com/julienschmidt/httprouter
GOPATH=$(echo $(pwd)) go get -u github.com/rs/cors
#GOPATH=$(echo $(pwd)) go get -u -t github.com/volatiletech/sqlboiler
GOPATH=$(echo $(pwd)) go get -u github.com/go-sql-driver/mysql
GOPATH=$(echo $(pwd)) go get -u gopkg.in/volatiletech/null.v6
GOPATH=$(echo $(pwd)) go get -u github.com/jmoiron/sqlx
GOPATH=$(echo $(pwd)) go get -u github.com/dgrijalva/jwt-go
GOPATH=$(echo $(pwd)) go get -u github.com/ahmetb/go-linq
GOPATH=$(echo $(pwd)) go get -u github.com/pkg/errors
