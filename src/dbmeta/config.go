package dbmeta

import (
	"os"
	"log"
	"bufio"
	"strings"
	"path/filepath"
	"github.com/jmoiron/sqlx"
	_ "github.com/go-sql-driver/mysql"
)
var MainDB = GetSqlxDriver()
func GetSqlxDriver() *sqlx.DB {
	db, err := sqlx.Connect(GetSqlDriver())
	if err != nil {
		panic(err.Error())
	}
	log.Println("Database connection is verified")
	return db
	//return "mysql", "user=qwer password=qwer1234 dbname=p009 sslmode=disable"
}

func GetSqlDriver() (string, string){
	spath, err := filepath.Abs("./dbconfig.txt")
	if err != nil {
		log.Fatal(err)
	}
	file, err := os.Open(spath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var sdriver string
	var sdsn string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		sa := strings.Split(scanner.Text(), " = ")
		if (len(sa) == 2) {
			if (sa[0] == "db.driver") {
				sdriver = strings.Trim(sa[1], "\"")
			} else if (sa[0] == "db.dsn") {
				sdsn = strings.Trim(sa[1], "\"")
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return sdriver, sdsn
}
