package api

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
	"fmt"
	"dbmeta"
	"encoding/json"
	"io/ioutil"
	"lib"
	"log"
)

//Assume email is the key identify record. But it is bad because it is going to embedded in URL and not safe in transmission. Consider other approach in real world.
const rlist = "/addresses"
const radd = "/address/new"
const rquery = "/address/:email"
const rmodify = "/address/mod"
const rdel = "/address/del/:email"

func Rlist(router *httprouter.Router) {
	router.GET(rlist, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		defer func() {
			if rr := recover(); rr != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprint(w, "Service exception. Recovered...")
			}
		}()

		var slice []*lib.Addr
		var dberr = dbmeta.MainDB.Select(&slice, "select first, last, email, phone from address")
		if dberr != nil {
			w.WriteHeader(http.StatusExpectationFailed)
			fmt.Fprint(w, "SQL error. Check DB")
			return
		}

		var o, e = json.Marshal(slice)
		if e != nil {
			w.WriteHeader(http.StatusExpectationFailed)
			fmt.Fprint(w, "JSON failed")
			return
		}
		s := string(o)
		w.WriteHeader(http.StatusOK)
		fmt.Fprint(w, s)
	})
	log.Println("Service list address(es) started")
}
func Rquery(router *httprouter.Router) {
	router.GET(rquery, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		defer func() {
			if rr := recover(); rr != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprint(w, "Service exception. Recovered...")
			}
		}()
		vEmail := p.ByName("email")
		if vEmail == "" {
			w.WriteHeader(http.StatusExpectationFailed)
			fmt.Fprint(w, "Please specify email to query address")
			return
		}
		//Shall check SQL injection here.
		var slice []*lib.Addr
		var dberr = dbmeta.MainDB.Select(&slice, "select first, last, email, phone from address where email = ?", vEmail)
		if dberr != nil {
			w.WriteHeader(http.StatusExpectationFailed)
			fmt.Fprint(w, "SQL error or no records found. Check DB")
			return
		}
		if len(slice) > 1 {
			w.WriteHeader(http.StatusExpectationFailed)
			fmt.Fprint(w, "More than one addres found for one email: "+vEmail)
			return
		}
		var o, e = json.Marshal(slice[0])
		if e != nil {
			w.WriteHeader(http.StatusExpectationFailed)
			fmt.Fprint(w, "JSON failed")
			return
		}
		s := string(o)
		w.WriteHeader(http.StatusOK)
		fmt.Fprint(w, s)
	})
	log.Println("Service query address started")
}
func addParser(w http.ResponseWriter, r *http.Request) (lib.Addr, error) {
	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return lib.Addr{}, err
	}
	// Unmarshal
	var vAddr lib.Addr
	err = json.Unmarshal(b, &vAddr)
	if err != nil {
		return lib.Addr{}, err
	}
	return vAddr, nil
}
func Radd(router *httprouter.Router) {
	router.POST(radd, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		defer func() {
			if rr := recover(); rr != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprint(w, "Service exception. Recovered...")
			}
		}()
		vAddr, err := addParser(w, r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		_, dberr := dbmeta.MainDB.Exec("insert into address (first, last, email, phone) values(?, ?, ?, ?)", vAddr.First, vAddr.Last, vAddr.Email, vAddr.Phone)
		if dberr != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprint(w, dberr.Error())
			return
		}
	})
	log.Println("Service create new address started")
}
func Rmodify(router *httprouter.Router) {
	router.PUT(rmodify, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		defer func() {
			if rr := recover(); rr != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprint(w, "Service exception. Recovered...")
			}
		}()
		vAddr, err := addParser(w, r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		_, dberr := dbmeta.MainDB.Exec("update address (first, last, email, phone) values(?, ?, ?, ?)", vAddr.First, vAddr.Last, vAddr.Email, vAddr.Phone)
		if dberr != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprint(w, dberr.Error())
			return
		}
	})
	log.Println("Service modify address started")
}
func Rdel(router *httprouter.Router) {
	router.DELETE(rdel, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		defer func() {
			if rr := recover(); rr != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprint(w, "Service exception. Recovered...")
			}
		}()
		vEmail := p.ByName("email")
		if vEmail == "" {
			w.WriteHeader(http.StatusExpectationFailed)
			fmt.Fprint(w, "Please specify email to query address")
			return
		}
		_, dberr := dbmeta.MainDB.Exec("delete from address where email = ?", vEmail)
		if dberr != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprint(w, dberr.Error())
			return
		}
	})
	log.Println("Service delete address started")
}
