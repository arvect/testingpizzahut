package api

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
	"fmt"
)

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "Welcome. This is a demo for Pizzahut!\n")
}

func Router() *httprouter.Router {
	router := httprouter.New()
	router.GET("/", Index)
	Rlist(router)
	Radd(router)
	Rmodify(router)
	Rquery(router)
	Rdel(router)
	return router
}

