package main

import (
	"net/http"
	"log"
	"api"
	"github.com/rs/cors"
)
func main() {
	var router = api.Router()
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"http://localhost", "http://localhost:8080", "https://www.xxxxxx.xyz", "https://xxxxx.xyz"},
		AllowedHeaders: []string{"Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowCredentials: true,
		// Enable Debugging for testing, consider disabling in production
		Debug: true,
	})
	handler := c.Handler(router)
	log.Println("Restful service for pizzahut test started successfully at 9000...")
	log.Fatal(http.ListenAndServe(":9000", handler))
}